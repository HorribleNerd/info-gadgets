package com.horriblenerd.infogadgets.gadgets;

import com.horriblenerd.infogadgets.InfoGadgets;
import com.horriblenerd.infogadgets.util.GadgetBase;
import com.horriblenerd.infogadgets.util.HudPosition;
import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.MainWindow;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.StringTextComponent;

/**
 * Created by HorribleNerd on 25/03/2021
 */
public class GadgetCompass extends GadgetBase {

    public GadgetCompass() {
        super(HudPosition.DEFAULT, true);
    }

    @Override
    public StringTextComponent getText(ClientPlayerEntity player) {
        int yaw = (int) MathHelper.wrapDegrees(player.rotationYaw);

        StringBuilder builder = new StringBuilder();
        builder.append('<');
        for (int i = yaw - 95; i < yaw + 95; i++) {
            switch (Math.abs(i) % 360) {
                case 90:
                    builder.append("W");
                    break;
                case 180:
                    builder.append("N");
                    break;
                case 270:
                    builder.append("E");
                    break;
                case 45:
                    builder.append("SW");
                    break;
                case 135:
                    builder.append("NW");
                    break;
                case 225:
                    builder.append("NE");
                    break;
                case 315:
                    builder.append("SE");
                    break;
                case 0:
                case 360:
                    builder.append("S");
                    break;
                default:
                    if (i % 10 == 0) {
                        builder.append('-');
                    }
                    break;
            }
        }
        builder.append('>');



        return new StringTextComponent(builder.toString());
    }

    @Override
    public void drawString(MatrixStack matrixStack, FontRenderer fontRenderer, MainWindow window, ClientPlayerEntity player) {
        StringTextComponent text = getText(player);
        fontRenderer.func_243246_a(matrixStack, text, window.getScaledWidth() / 2f - text.getText().length() * 3f, 5, InfoGadgets.DEFAULT_COLOR.getColor());

        StringTextComponent text2 = new StringTextComponent(String.format("%.01f", MathHelper.wrapDegrees(player.rotationYaw)));
        fontRenderer.func_243246_a(matrixStack, text2, window.getScaledWidth() / 2f - text2.getText().length() * 3f, 15, InfoGadgets.DEFAULT_COLOR.getColor());
    }
}
