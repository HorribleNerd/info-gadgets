package com.horriblenerd.infogadgets.gadgets;

import com.horriblenerd.infogadgets.util.GadgetBase;
import com.horriblenerd.infogadgets.util.HudPosition;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.util.text.StringTextComponent;

/**
 * Created by HorribleNerd on 25/03/2021
 */
public class GadgetTime extends GadgetBase {

    public GadgetTime() {
        super(HudPosition.DEFAULT, false);
    }

    @Override
    public StringTextComponent getText(ClientPlayerEntity player) {
        float ticks = (player.worldClient.getDayTime() + 6000) % 24_000;
        return new StringTextComponent(String.format("%02d:%02d", (int) ticks / 1000, (int) (ticks / (1000f / 60f)) % 60));
    }
}
