package com.horriblenerd.infogadgets.gadgets;

import com.horriblenerd.infogadgets.util.GadgetBase;
import com.horriblenerd.infogadgets.util.HudPosition;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.entity.EntityPredicate;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.monster.CreeperEntity;
import net.minecraft.entity.passive.MooshroomEntity;
import net.minecraft.entity.passive.SheepEntity;
import net.minecraft.item.DyeColor;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.text.StringTextComponent;

import java.util.ArrayList;
import java.util.function.Predicate;

/**
 * Created by HorribleNerd on 25/03/2021
 */
public class GadgetRareMobs extends GadgetBase {

    public static final ArrayList<EntityType<?>> ENTITIES = new ArrayList<>();

    public GadgetRareMobs() {
        super(HudPosition.DEFAULT, false);
        ENTITIES.add(EntityType.WITHER_SKELETON);
        ENTITIES.add(EntityType.WANDERING_TRADER);
        ENTITIES.add(EntityType.SKELETON_HORSE);
        ENTITIES.add(EntityType.ZOMBIE_HORSE);
        ENTITIES.add(EntityType.ZOMBIE_VILLAGER);
        ENTITIES.add(EntityType.WITCH);
        ENTITIES.add(EntityType.PILLAGER);
        ENTITIES.add(EntityType.RAVAGER);
        ENTITIES.add(EntityType.SHULKER);
        ENTITIES.add(EntityType.VINDICATOR);
        ENTITIES.add(EntityType.VEX);
        ENTITIES.add(EntityType.ELDER_GUARDIAN);
        ENTITIES.add(EntityType.WITHER);
        ENTITIES.add(EntityType.ENDER_DRAGON);
    }

    @Override
    public StringTextComponent getText(ClientPlayerEntity player) {
        EntityPredicate entityPredicate = new EntityPredicate().setCustomPredicate(new Predicate<LivingEntity>() {
            @Override
            public boolean test(LivingEntity livingEntity) {
                if (ENTITIES.contains(livingEntity.getType())) {
                    return true;
                }
                else if (livingEntity instanceof CreeperEntity && ((CreeperEntity) livingEntity).isCharged()) {
                    return true;
                }
                else if (livingEntity instanceof MooshroomEntity && ((MooshroomEntity) livingEntity).getMooshroomType() == MooshroomEntity.Type.BROWN) {
                    return true;
                }
                else if (livingEntity instanceof SheepEntity && ((SheepEntity) livingEntity).getFleeceColor() == DyeColor.PINK) {
                    return true;
                }
                return false;
            }
        });
        LivingEntity closestEntity = player.world.getClosestEntity(player.world.getLoadedEntitiesWithinAABB(
                LivingEntity.class,
                new AxisAlignedBB(player.getPosX() - 16, player.getPosY() - 16, player.getPosZ() - 16, player.getPosX() + 16, player.getPosY() + 16, player.getPosZ() + 16)),
                entityPredicate,
                null,
                32, 32, 32);
//        player.world.getClosestEntityWithinAABB()
        if (closestEntity == null) {
            return new StringTextComponent("Nothing");
        }
        return new StringTextComponent(String.format("%s", closestEntity.getName().getString()));
    }
}
