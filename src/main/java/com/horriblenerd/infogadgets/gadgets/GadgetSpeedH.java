package com.horriblenerd.infogadgets.gadgets;

import com.horriblenerd.infogadgets.util.GadgetBase;
import com.horriblenerd.infogadgets.util.HudPosition;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.text.StringTextComponent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

/**
 * Created by HorribleNerd on 25/03/2021
 */
public class GadgetSpeedH extends GadgetBase {

    private static final Logger LOGGER = LogManager.getLogger();

    public GadgetSpeedH() {
        super(HudPosition.DEFAULT, false);
    }

    @Override
    public StringTextComponent getText(ClientPlayerEntity player) {
        float v1 = (float) Math.sqrt(player.getDistanceSq(player.prevPosX, player.getPosY(), player.prevPosZ)) * 20;
        return new StringTextComponent(String.format("%.3f m/s (%.1f km/h)", v1, v1 * 3.6));
    }

}
