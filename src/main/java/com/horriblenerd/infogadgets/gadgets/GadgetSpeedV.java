package com.horriblenerd.infogadgets.gadgets;

import com.horriblenerd.infogadgets.util.GadgetBase;
import com.horriblenerd.infogadgets.util.HudPosition;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.util.text.StringTextComponent;

/**
 * Created by HorribleNerd on 25/03/2021
 */
public class GadgetSpeedV extends GadgetBase {

    public GadgetSpeedV() {
        super(HudPosition.DEFAULT, false);
    }

    @Override
    public StringTextComponent getText(ClientPlayerEntity player) {
        float v2 = (float) Math.sqrt((player.getDistanceSq(player.getPosX(), player.prevPosY, player.getPosZ()))) * 20;
        return new StringTextComponent(String.format("%.3f m/s (%.1f km/h)", v2, v2 * 3.6));
    }
}
