package com.horriblenerd.infogadgets.gadgets;

import com.horriblenerd.infogadgets.InfoGadgets;
import com.horriblenerd.infogadgets.util.GadgetBase;
import com.horriblenerd.infogadgets.util.HudPosition;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.text.StringTextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.LogicalSide;
import net.minecraftforge.fml.common.Mod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.IntStream;

/**
 * Created by HorribleNerd on 25/03/2021
 */
//@Mod.EventBusSubscriber(value = Dist.CLIENT, modid = InfoGadgets.MODID, bus = Mod.EventBusSubscriber.Bus.FORGE)
public class GadgetSpeedExtra extends GadgetBase {

    private static final Logger LOGGER = LogManager.getLogger();

    private final ArrayList<Float> speeds;
    private static final int MEMORY_SIZE = 100;
    private int index = 0;

    public GadgetSpeedExtra() {
        super(HudPosition.DEFAULT, false);
        speeds = new ArrayList<>(MEMORY_SIZE);
        IntStream.range(0, MEMORY_SIZE).mapToObj(i -> 0f).forEach(speeds::add);

        MinecraftForge.EVENT_BUS.register(this);
    }

    @Override
    public StringTextComponent getText(ClientPlayerEntity player) {
        float v1 = (float) Math.sqrt(player.getDistanceSq(player.prevPosX, player.getPosY(), player.prevPosZ)) * 20;
        return new StringTextComponent(String.format("top: %.3f m/s, avg: %.3f m/s", Collections.max(speeds), (float) speeds.stream().mapToDouble(a -> a).average().getAsDouble()));
    }

    @Override
    public void enable() {
        super.enable();
        Collections.fill(speeds, 0f);
    }

    @SubscribeEvent
    public void onTick(TickEvent.ClientTickEvent event) {
        if (event.phase != TickEvent.Phase.END) return;
        ClientPlayerEntity player = Minecraft.getInstance().player;
        if (player == null) return;
        float v1 = (float) Math.sqrt(player.getDistanceSq(player.prevPosX, player.getPosY(), player.prevPosZ)) * 20;
        speeds.set(index, v1);
        index++;
        if (index >= MEMORY_SIZE) index = 0;
    }

}
