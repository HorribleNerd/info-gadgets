package com.horriblenerd.infogadgets.gadgets;

import com.horriblenerd.infogadgets.util.GadgetBase;
import com.horriblenerd.infogadgets.util.HudPosition;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.util.text.StringTextComponent;

/**
 * Created by HorribleNerd on 25/03/2021
 */
public class GadgetCoordinates extends GadgetBase {

    public GadgetCoordinates() {
        super(HudPosition.DEFAULT, false);
    }

    @Override
    public StringTextComponent getText(ClientPlayerEntity player) {
        return new StringTextComponent(String.format("%.0f x, %.0f y, %.0f z", player.getPosX(), player.getPosY(), player.getPosZ()));
    }
}
