package com.horriblenerd.infogadgets.gadgets;

import com.horriblenerd.infogadgets.util.GadgetBase;
import com.horriblenerd.infogadgets.util.HudPosition;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.item.Food;
import net.minecraft.item.Item;
import net.minecraft.util.FoodStats;
import net.minecraft.util.text.StringTextComponent;

/**
 * Created by HorribleNerd on 25/03/2021
 */
public class GadgetHunger extends GadgetBase {

    public GadgetHunger() {
        super(HudPosition.DEFAULT, false);
    }

    @Override
    public StringTextComponent getText(ClientPlayerEntity player) {
        FoodStats foodStats = player.getFoodStats();

        String foodString;

        Item item1 = player.getHeldItemMainhand().getItem();
        Food food;
        food = item1.getFood();
        if (food == null) {
            Item item2 = player.getHeldItemOffhand().getItem();
            food = item2.getFood();

        }
        if (food != null) {
            foodString = String.format("Hunger: %d (+%d), Saturation %.1f (+%.1f)", foodStats.getFoodLevel(), food.getHealing(), foodStats.getSaturationLevel(), food.getSaturation());
        }
        else {
            foodString = String.format("Hunger: %d, Saturation %.1f", foodStats.getFoodLevel(), foodStats.getSaturationLevel());
        }

        return new StringTextComponent(foodString);
    }
}
