package com.horriblenerd.infogadgets.util;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.MainWindow;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.text.StringTextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

/**
 * Created by HorribleNerd on 20/03/2021
 */
@OnlyIn(Dist.CLIENT)
public interface IGadget {

    StringTextComponent getText(ClientPlayerEntity player);


    /**
     * @return true if this gadget ignores Y offset and uses a static HudPosition instead
     */
    boolean isSpecial();

    float getYOffset();

    void setHudPosition(HudPosition pos);

    HudPosition getHudPosition(MainWindow window);

    HudPosition getDefaultHudPosition();

    void setX(float x);

    void setY(float y);

    boolean isEnabled();

    void enable();

    void disable();

    void toggle();

    /**
     * Only called when isSpecial() returns true
     */
    void drawString(MatrixStack matrixStack, FontRenderer fontRenderer, MainWindow window, ClientPlayerEntity player);
}
