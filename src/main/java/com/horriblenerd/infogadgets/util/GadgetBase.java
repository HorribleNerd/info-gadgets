package com.horriblenerd.infogadgets.util;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.MainWindow;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.util.text.StringTextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

/**
 * Created by HorribleNerd on 25/03/2021
 */
@OnlyIn(Dist.CLIENT)
public class GadgetBase implements IGadget {

    private final HudPosition defaultPos;
    private HudPosition pos;

    private final boolean isSpecial;
    private boolean enabled = false;

    public GadgetBase(HudPosition pos, boolean isSpecial) {
        this.defaultPos = pos;
        this.pos = pos;
        this.isSpecial = isSpecial;
    }

    @Override
    public StringTextComponent getText(ClientPlayerEntity player) {
        return new StringTextComponent("");
    }

    @Override
    public boolean isSpecial() {
        return isSpecial;
    }

    @Override
    public float getYOffset() {
        return isSpecial() ? 0 : 10;
    }

    @Override
    public void setHudPosition(HudPosition pos) {
        this.pos = pos;
    }

    @Override
    public HudPosition getHudPosition(MainWindow window) {
        return pos;
    }

    @Override
    public HudPosition getDefaultHudPosition() {
        return defaultPos;
    }

    @Override
    public void setX(float x) {
        pos.setX(x);
    }

    @Override
    public void setY(float y) {
        pos.setY(y);
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void enable() {
        this.enabled = true;
    }

    @Override
    public void disable() {
        this.enabled = false;
    }

    @Override
    public void toggle() {
        if (enabled) {
            disable();
        }
        else {
            enable();
        }
    }

    @Override
    public void drawString(MatrixStack matrixStack, FontRenderer fontRenderer, MainWindow window, ClientPlayerEntity player) {

    }
}
