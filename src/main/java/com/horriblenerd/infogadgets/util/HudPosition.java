package com.horriblenerd.infogadgets.util;

/**
 * Created by HorribleNerd on 25/03/2021
 */
public class HudPosition {

    public static final HudPosition DEFAULT = new HudPosition(2, 5);

    private float x;
    private float y;

    public HudPosition(float x, float y) {
        this.x = x;
        this.y = y;
    }


    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }
}
