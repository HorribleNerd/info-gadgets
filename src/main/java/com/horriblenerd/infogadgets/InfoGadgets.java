package com.horriblenerd.infogadgets;

import com.horriblenerd.infogadgets.gadgets.*;
import com.horriblenerd.infogadgets.util.HudPosition;
import com.horriblenerd.infogadgets.util.IGadget;
import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.MainWindow;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.util.text.Color;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

// The value here should match an entry in the META-INF/mods.toml file
@Mod(InfoGadgets.MODID)
public class InfoGadgets {
    // Directly reference a log4j logger.
    private static final Logger LOGGER = LogManager.getLogger();
    public static final Color DEFAULT_COLOR = Color.fromInt(14737632);
    public static final String MODID = "infogadgets";


    private ArrayList<IGadget> Gadgets = new ArrayList<>();

    public InfoGadgets() {
        // Register ourselves for server and other game events we are interested in

        initDefaultGadgets();

        for (IGadget g : Gadgets) {
            g.enable(); // DEBUG
        }

        MinecraftForge.EVENT_BUS.register(this);
    }

    private void initDefaultGadgets() {
        addGadget(new GadgetTime());
        addGadget(new GadgetSpeedH());
        addGadget(new GadgetSpeedV());
        addGadget(new GadgetSpeedExtra());
        addGadget(new GadgetCoordinates());
        addGadget(new GadgetCompass());
        addGadget(new GadgetHunger());
        addGadget(new GadgetExp());
        addGadget(new GadgetRareMobs());
        addGadget(new GadgetMobCount());
    }

    public void addGadget(IGadget gadget) {
        Gadgets.add(gadget);
    }

    @OnlyIn(Dist.CLIENT)
    @SubscribeEvent
    public void onRenderGameOverlay(RenderGameOverlayEvent.Text event) {
        Minecraft mc = Minecraft.getInstance();
        MatrixStack matrixStack = event.getMatrixStack();
        if (mc.world != null && mc.player != null) {
            ClientWorld world = mc.world;
            ClientPlayerEntity player = mc.player;
            FontRenderer fontRenderer = mc.fontRenderer;
            MainWindow window = event.getWindow();

            HudPosition start = HudPosition.DEFAULT;
            float x = start.getX();
            float y = start.getY();
            for (IGadget g : Gadgets) {
                if (g.isEnabled()) {
                    if (g.isSpecial()) {
                        g.drawString(matrixStack, fontRenderer, window, player);
                    }
                    else {
                        drawString(matrixStack, fontRenderer, g.getText(player), x, y, DEFAULT_COLOR);
                        y += g.getYOffset();
                    }
                }
            }

        }
    }

    public static void drawString(MatrixStack matrixStack, FontRenderer fontRenderer, ITextComponent string, float x, float y, Color color) {
        fontRenderer.func_243246_a(matrixStack, string, x, y, color.getColor());
    }

}
